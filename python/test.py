import cv2
import svar
import time
import numpy as np

zbar=svar.load('svar_zbar')

scanner=zbar.Scanner()

img=cv2.imread('qrcode-feature.webp')
img=cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)

t=time.time()
ret=scanner.scan(img.data)
print(ret,time.time()-t)

mat=np.array(scanner.display("image",img.data,ret))
cv2.imshow('mat',mat)
cv2.waitKey(0)
